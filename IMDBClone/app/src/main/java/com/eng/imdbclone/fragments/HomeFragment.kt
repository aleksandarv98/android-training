package com.eng.imdbclone.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.eng.imdbclone.DataGlobal
import com.eng.imdbclone.DataGlobal.movieList
import com.eng.imdbclone.DataGlobal.objectArray
import com.eng.imdbclone.HomeTopSliderAdapter
import com.eng.imdbclone.JsonDataAdapter
import com.eng.imdbclone.R
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        createDataSource()

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val adapter = HomeTopSliderAdapter(objectArray)
        recyclerView!!.adapter = adapter
        recyclerView!!.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        val funAdapter = JsonDataAdapter(movieList, 1)
        recyclerViewFanFavorites!!.adapter = funAdapter
        recyclerViewFanFavorites!!.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
    }

    private fun createDataSource() {
        objectArray.add(
            DataGlobal.Banner(
                "Fast and Furious 9",
                "Final episode",
                R.drawable.fast_9,
                R.drawable.fast_and_furious9
            )
        )
        objectArray.add(
            DataGlobal.Banner(
                "I'm Legend",
                "Trailer",
                R.drawable.im_a_legend_backgorund,
                R.drawable.im_a_legend
            )
        )
        objectArray.add(
            DataGlobal.Banner(
                "The Walking Dead",
                "AMC Series",
                R.drawable.the_walking_dead_background,
                R.drawable.the_walking_dead
            )
        )
        objectArray.add(
            DataGlobal.Banner(
                "Escape Room",
                "Find the clues or die",
                R.drawable.escape_room_background,
                R.drawable.escape_room
            )
        )
        objectArray.add(
            DataGlobal.Banner(
                "Jhonny English Reborn",
                "Jhonny returns",
                R.drawable.jhonny_english_reborn_background,
                R.drawable.johnny_english_reborn
            )
        )
    }

}