@file:Suppress("DEPRECATION")

package com.eng.imdbclone

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import org.json.JSONObject
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val jsonURL = "https://imdb-api.com/en/API/Top250Movies/k_tndu3gjt"
        getJSON().execute(jsonURL)

        val donjaNavigacija = findViewById<BottomNavigationView>(R.id.bottomNavigationView)
        val sadrzajFragmenta = findNavController(R.id.fragment)

        donjaNavigacija.setupWithNavController(sadrzajFragmenta)
    }

    inner class getJSON(): AsyncTask<String, String, String>() {
        override fun doInBackground(vararg params: String?): String {
            val json: String
            val connection = URL(params[0]).openConnection() as HttpURLConnection
            try {
                connection.connect()
                json = connection.inputStream.use {
                    it.reader().use {
                            reader -> reader.readText()
                    }
                }
            } finally {
                connection.disconnect()
            }
            return json
        }
        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            getDataFromJSON(result)
        }
    }

    fun getDataFromJSON(json: String?) {
        val jsonObject = JSONObject(json)
        val movies = jsonObject.getJSONArray("items")

        movies.let {
            (0 until it.length()).forEach {
                if (it < DataGlobal.maxMovies) {
                    val movie = movies.getJSONObject(it)

                    val id = movie.getString("id")
                    val rank = movie.getInt("rank")
                    val title = movie.getString("title")
                    val fullTitle = movie.getString("fullTitle")
                    val year = movie.getInt("year")
                    val imageURL = movie.getString("image")
                    val crew = movie.getString("crew")
                    val imDbRating = movie.getDouble("imDbRating")
                    val imDbRatingCount = movie.getInt("imDbRatingCount")

                    val image: Bitmap? = null
                    val movieObject = DataGlobal.Movie(id, rank, title, fullTitle, year, image, crew, imDbRating, imDbRatingCount)

                    getImageFromJSON(movieObject).execute(imageURL.replace("original", "192x264"))
                }
            }
        }
    }

    inner class getImageFromJSON(private val movie: DataGlobal.Movie) : AsyncTask<String, Void, Bitmap?>() {
        override fun doInBackground(vararg params: String?): Bitmap? {
            var image: Bitmap? = null
            val urlAddress = params[0]

            try {
                val photoFromInternet = URL(urlAddress).openStream()
                image = BitmapFactory.decodeStream(photoFromInternet)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return image
        }

        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)

            movie.image = result
            DataGlobal.movieList.add(movie)
        }

    }

}