package com.eng.imdbclone

import android.graphics.Bitmap

object DataGlobal {

    val objectArray: ArrayList<Banner> = arrayListOf()

    val movieList : ArrayList<Movie> = arrayListOf()

    const val maxMovies = 30
    var topPicksSection = 1
    var streamingSection = 2
    var exploreMoviesAndTvSection = 3

    data class Movie(
        val id: String,
        val rank: Int,
        val title: String,
        val fullTitle: String,
        val year: Int,
        var image: Bitmap?,
        val crew: String,
        val imDbRating: Double,
        val imDbRatingCount: Int
    )

    data class Banner(
        val title: String, val subtitle: String,
        val backgroundPicture: Int, val frontPicture: Int
    )
}