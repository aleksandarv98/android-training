package com.eng.imdbclone

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.cell_home_top_slider.view.*

class HomeTopSliderAdapter(private val getData: ArrayList<DataGlobal.Banner>) :
    RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.cell_home_top_slider, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.naslov.text = getData[position].title
        holder.view.podnaslov.text = getData[position].subtitle
        holder.view.bannerPhoto.setImageResource(getData[position].backgroundPicture)
        holder.view.smallPictureNextToBannerPhoto.setImageResource(getData[position].frontPicture)
    }

    override fun getItemCount(): Int {
        return getData.count()
    }
}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view){

}
