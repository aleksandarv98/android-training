package com.eng.imdbclone

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class JsonDataAdapter(val receivedData: MutableList<DataGlobal.Movie>, val sectionChecker: Int) :
    RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.cell_movie, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val image = holder.view.findViewById<ImageView>(R.id.posterPicture)
        image.setImageBitmap(receivedData[position].image)

        val rating = holder.view.findViewById<TextView>(R.id.rate)
        rating.text = receivedData[position].imDbRating.toString()

        if (receivedData[position].imDbRating.toString() == "") {
            val starImage = holder.view.findViewById<ImageView>(R.id.rateStar)
            starImage.visibility = View.GONE
        }

        val title = holder.view.findViewById<TextView>(R.id.name)
        title.text = receivedData[position].title

        val year = holder.view.findViewById<TextView>(R.id.year)
        year.text = receivedData[position].year.toString()

        val button = holder.view.findViewById<Button>(R.id.button)
        if (position % 2 == 0) {
            button.text = "+ Watchlist"
        } else {
            button.text = "Showtimes"
        }

        when (sectionChecker) {
            DataGlobal.topPicksSection -> {
                val infoIcon = holder.view.findViewById<ImageView>(R.id.movieInfoImageView)
                infoIcon.visibility = View.VISIBLE
            }
            DataGlobal.streamingSection -> {
                button.text = "Watch now"
            }
            DataGlobal.exploreMoviesAndTvSection -> {
                button.text = "Showtimes"
            }
        }
    }

    override fun getItemCount(): Int {
        return receivedData.count()
    }
}