package com.eng.audievent

data class PremiumEvent(
    val id: String,
    val title: String,
    val description: PremiumEventDescription,
    val headerPremium: String,
    val linkMyAudiPremium: ArrayList<LinkMyAudi>,
    val programNotes: ProgramNotes,
    val detailedProgram: ArrayList<EventDay>
    ) {
}

data class EventDay(
    val day: String,
    val activities: ArrayList<DailyActivity>,
    val subtitle: String,
    val image: BackgroundImage
    ) {

}

data class DailyActivity(
    val start: String,
    val end: String,
    val activity: String
    ) {

}

data class ProgramNotes(
    val value: String,
    val format: String,
    val processed: String
    ) {

}

data class PremiumEventDescription(
    override val format: String,
    override val value: String,
    val processed: String
    ) : EventDescription {

}
