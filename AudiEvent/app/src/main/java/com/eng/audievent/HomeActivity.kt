package com.eng.audievent

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView

val URL = "https://www.audi.it"

class HomeActivity : AppCompatActivity() {
    private lateinit var toggle: ActionBarDrawerToggle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val drawerLayout = findViewById<DrawerLayout>(R.id.drawerLayout)

        toggle = ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        val navMenuIcon = findViewById<ImageView>(R.id.navMenuIcon)
        val navView = findViewById<NavigationView>(R.id.navView)

        navMenuIcon.setOnClickListener {
            drawerLayout.openDrawer(navView)
        }

        navView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.home -> Toast.makeText(this, "Home item", Toast.LENGTH_SHORT).show()
                R.id.calendar -> Toast.makeText(this, "Calendar item", Toast.LENGTH_SHORT).show()
                R.id.coupon -> Toast.makeText(this, "Coupon item", Toast.LENGTH_SHORT).show()
            }
            true
        }

        val navFooterView = findViewById<NavigationView>(R.id.navFooterView)

        navFooterView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.impostazioni -> startActivity(Intent(this, SettingsActivity::class.java))
            }
            true
        }

        val gammaAudiLayout = findViewById<LinearLayout>(R.id.gamma_audi_layout)

        gammaAudiLayout.setOnClickListener {
            val uri = Uri.parse("googlechrome://navigate?url=$URL")
            startActivity(Intent(Intent.ACTION_VIEW, uri))
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}