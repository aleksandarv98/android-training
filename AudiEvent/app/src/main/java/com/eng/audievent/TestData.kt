package com.eng.audievent

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception
import java.net.URL
import java.time.LocalDate

class TestData : AppCompatActivity() {

    var image: Bitmap? = null

    fun populateTestData(){
        loadJsonDataPublicEvent()
        loadJsonDataCalendarEvent()
        readDataProgramma()
        readAudiDrivingExperienceJSON()
        readFoodExperienceJson()
        readPlaceJSON()
        readContactAndInfoJSON()
        readNotificationJSON()
    }

    val publicEventsModelJson: String =
        """[
            |{ 
            |   'id': '1', 
            |   'titolo': 'Audi Talks Performance: ieri, oggi e domani.', 
            |   'descrizione': { 
            |       'format': 'testo_formattato', 
            |       'value': 'Otto puntate con ospiti d\'eccezione per condividere visioni all\'avanguardia e discutere di una nuova, straordinaria idea'
            |   }, 
            |   'data_evento': '2021-02-27', 
            |   'link_myaudi': { 
            |       'uri': '', 
            |       'title': '' , 
            |       'options': []
            |   }, 
            |   'priorita': 1, 
            |   'stato': 'active', 
            |   'immagine_background': { 
            |       'id': '1', 
            |       'href': 'https://cdn.motor1.com/images/mgl/ogjQ4/s1/audi-r8-v10-plus-with-performance-parts.jpg', 
            |       'meta': { 
            |           'alt': '', 
            |           'title': '', 
            |           'width': 1170, 
            |           'height': 507
            |       } 
            |   } 
            }
          ]""".trimMargin()

    fun loadJsonDataPublicEvent() {

        val jsonArray = JSONArray(publicEventsModelJson)
        jsonArray.let {
            (0 until it.length()).forEach {
                val jsonObject = jsonArray.getJSONObject(it)
                val id = jsonObject.getString("id")
                val title = jsonObject.getString("titolo")
                val description = jsonObject.getJSONObject("descrizione")
                val value = description.getString("value")
                val format = description.getString("format")
                val eventDate = jsonObject.getString("data_evento")
                val linkMyAudi = jsonObject.getJSONObject("link_myaudi")
                val uri = linkMyAudi.getString("uri")
                val linkMyAudiTitle = linkMyAudi.getString("title")
                val options = linkMyAudi.getJSONArray("options")
                val optionsArrayList: ArrayList<String> = arrayListOf()
                /* TODO Insert options into array */
                options.let {
                    (0 until it.length()).forEach {
//                        optionsArrayList.add(jsonArray.getJSONObject(it))
                    }
                }
                val priority = jsonObject.getInt("priorita")
                val status = jsonObject.getString("stato")
                val backgroundImage = jsonObject.getJSONObject("immagine_background")
                val backgroundImageId = backgroundImage.getString("id")
                val href = backgroundImage.getString("href")
                val meta = backgroundImage.getJSONObject("meta")
                val metaAlt = meta.getString("alt")
                val metaTitle = meta.getString("title")
                val metaWidth = meta.getInt("width")
                val metaHeight = meta.getInt("height")

                val publicEvent = PublicEvent(
                    id,
                    title,
                    PublicEventDescription(
                        value,
                        format
                    ),
                    LocalDate.parse(eventDate),
                    LinkMyAudi(
                        uri,
                        linkMyAudiTitle,
                        optionsArrayList
                    ),
                    priority,
                    status,
                    BackgroundImage(
                        backgroundImageId,
                        href,
                        Meta(
                            metaAlt,
                            metaTitle,
                            metaWidth,
                            metaHeight
                        )
                    )
                )
                DataGlobal.publicEventsList.add(publicEvent)
                loadImageFromURL().execute(publicEvent.backgroundImage.href)
                println("******")
                println(publicEvent)
                println("******")
            }
        }
    }

    val calendarEventModelJson: String = """
         [
            {
              "title": "Mountaineering Workshop con Herve",
              "description": "Un evento dedicato alla montagna e al giusto approccio per viverla con rispetto e intelligenza, dalle attivita sportive come lo sci...",
              "image": "https://queenstownmountainguides.co.nz/wp-content/uploads/2019/07/5-Day-Mountaineering-Expedition-Hero.jpg"
            },
            {
              "title": "Audi Talks Performance: ieri, oggi e domani",
              "description": "Otto puntate con ospiti d'eccezione per condividere visioni all'avanguardia e discutere di una nuova, straordinaria idea",
              "image": "https://farm8.staticflickr.com/7109/13657575034_e7d639ab5d_b.jpg"
            },
            {
              "title": "Cortina e-portrait",
              "description": "Dal 2017 Audi - per tutelare Cortina come patrimonio si e fatta promotrice con il Comune di Ampezzo di un progetto di Corporate Social",
              "image": "https://account.myaudi.it/api/v1/assets/images/013aeefc-8bae-4e00-840f-9fcb0bff0052/cortina-e-portrait.jpeg?mode=crop&w=1200x630"
            },
            {
              "title": "Performance Workshop con Kristian Ghedina",
              "description": "I pluricampioni Kristian Ghedina e Peter Fill, insieme agli chef stellati Costardi Bros, ti guideranno in un'esperienza totale, tra sport",
              "image": "https://i1.wp.com/techbeasts.com/wp-content/uploads/2016/05/mountain_nature_elegant_wallpaper_3f.jpg"
            },
            {
              "title": "Wanderlust con Klaus",
              "description": "il dj e creator Klaus portera a Cortina il suo porgetto Wanderlust, suonando all'alba su una delle piste piu famose: un'esperienza in",
              "image": ""
            }
         ]
    """.trimIndent()

    fun loadJsonDataCalendarEvent() {
        val calendarJsonArray = JSONArray(calendarEventModelJson)
        calendarJsonArray.let {
            (0 until it.length()).forEach {
                val jsonObject = calendarJsonArray.getJSONObject(it)
                val title = jsonObject.getString("title")
                val description = jsonObject.getString("description")
                val image = jsonObject.getString("image")

                val calendarEvent = AllDataForTest.CalendarEvent(
                    title,
                    description,
                    image
                )
                DataGlobal.calendarEventsList.add(calendarEvent)
                println(calendarEvent)
            }
        }
    }

    val programaJSONString = """{
	"image": "audi_eventi_program.jpg",
	"title": "Reconnect to nature con Hervé Barmasse",
    "sub_title":"Programma",
	"note": "*il programma potra subire varizaoni.",
	"description": "L’alpinista professionista Hervé Barmasse in una masterclass alla scoperta del profondo legame che lega gli esseri umani alla natura",
	"info_uri": "https://streaming.myaudi.it/eventi/barmasse",
	"daily_activity": [{
	"day": "Venerdi 26 Agosto",
	"activity":[{
	"start":"07:30",
	"end": "",
	"activity_name": "Benvenuto e brief tecnico con Hervé Barmasse"},
	{
	"start":"07:30",
	"end": "09:30",
	"activity_name": "Escursionismo"},
	{
	"start":"09:30",
	"end": "11:00",
	"activity_name": "Colazione in rifugio e inspirational talk"},
		{
	"start":"11:30",
	"end": "15:00",
	"activity_name": "Tempo libero"},
		{
	"start":"15:00",
	"end": "16:30",
	"activity_name": "Audi Driving Experience"},
	{
	"start":"Dalle",
	"end": "17:30",
	"activity_name": "Tempo libero e pernottamento"}
	]},
	{
	"day": "Sabato 28 Agosto",
	"activity":[{
	"start":"07:30",
	"end": "",
	"activity_name": "Benvenuto e brief tecnico con Hervé Barmasse"},
	{
	"start":"07:30",
	"end": "09:30",
	"activity_name": "Escursionismo"},
	{
	"start":"09:30",
	"end": "11:00",
	"activity_name": "Colazione in rifugio e inspirational talk"},
		{
	"start":"11:30",
	"end": "15:00",
	"activity_name": "Tempo libero"},
		{
	"start":"15:00",
	"end": "16:30",
	"activity_name": "Audi Driving Experience"},
	{
	"start":"Dalle",
	"end": "17:30",
	"activity_name": "Tempo libero e pernottamento"}
	]},
	{
	"day": "Domenica 29 Agosto",
	"activity":[{
	"start":"07:30",
	"end": "",
	"activity_name": "Benvenuto e brief tecnico con Hervé Barmasse"},
	{
	"start":"07:30",
	"end": "09:30",
	"activity_name": "Escursionismo"},
	{
	"start":"09:30",
	"end": "11:00",
	"activity_name": "Colazione in rifugio e inspirational talk"},
		{
	"start":"11:30",
	"end": "15:00",
	"activity_name": "Tempo libero"},
		{
	"start":"15:00",
	"end": "16:30",
	"activity_name": "Audi Driving Experience"},
	{
	"start":"Dalle",
	"end": "17:30",
	"activity_name": "Tempo libero e pernottamento"}
	]}]
	
}"""


    fun readDataProgramma() {

        val jsonObjProgram = JSONObject(programaJSONString)
        val listOfDailyActivity: ArrayList<AllDataForTest.DailyActivity> = arrayListOf()
        val listOfActivity: ArrayList<AllDataForTest.Activity> = arrayListOf()

        val image = jsonObjProgram.getString("image")
        val title = jsonObjProgram.getString("title")
        val subTitle = jsonObjProgram.getString("sub_title")
        val note = jsonObjProgram.getString("note")
        val description = jsonObjProgram.getString("description")
        val infoUri = jsonObjProgram.getString("info_uri")
        val dailyActivityList = jsonObjProgram.getJSONArray("daily_activity")

        dailyActivityList.let {
            (0 until it.length()).forEach {
                val dailyActivity = dailyActivityList.getJSONObject(it)
                val day = dailyActivity.getString("day")
                val activity = dailyActivity.getJSONArray("activity")

                activity.let {
                    (0 until it.length()).forEach {
                        val singleActivity = activity.getJSONObject(it)
                        val start = singleActivity.getString("start")
                        val end = singleActivity.getString("end")
                        val activityName = singleActivity.getString("activity_name")

                        var act = AllDataForTest.Activity(start, end, activityName)

                        listOfActivity.add(act)
                    }
                }
                var da: AllDataForTest.DailyActivity = AllDataForTest.DailyActivity(day, listOfActivity)

                listOfDailyActivity.add(da)
            }

        }
        val program =
            AllDataForTest.Program(
                image,
                title,
                subTitle,
                note,
                description,
                infoUri,
                listOfDailyActivity
            )

    }



    lateinit var placeObject: AllDataForTest.Places
    lateinit var audiDrivingExperienceObject: AllDataForTest.AudiDrivingExperience


    val place = """
        {
            "id": "1",
            "image_places": "https://cdn.tuttosport.com/img/990/495/2021/01/22/175858111-45480ee5-5fca-4368-a39f-e2b1c6940523.jpg",
            "title": "Audi e Madonna di Campiglio",
            "subtitle_places": "località perfetta per viviere un'esperienza a contatto con la natura",
            "title_places": "Madonna di Campiglio",
            "description": "Performance e sostenibilità nel cuore delle Dolomiti. Goditi l’estate in mezzo ai paesaggi incontaminati di Madonna di Campiglio: a bordo dei modelli elettrici Audi più innovativi, i percorsi tortuosi delle Dolomiti diventano il luogo perfetto per scoprire il nuovo rapporto tra tecnologia e natura della Casa dei quattro anelli. E dove vivere un’esperienza di guida nel pieno rispetto del territorio, ma anche della performance. Per salvaguardare le ricchezze di questa regione, patrimonio dell’Unesco, Audi si impegna ad avere entro il 2025 una gamma di veicoli sempre più sostenibile: 30 modelli a elevata elettrificazione, 20 dei quali totalmente elettrici. Vetture rivoluzionarie come Audi RS e-tron GT e Audi Q4 e-tron che già ci permettono di entrare in contatto con una nuova idea di progresso, in cui efficienza e rispetto per l’ambiente diventano priorità assolute. Scopri qui tutti gli eventi estivi Audi a Madonna di Campiglio!",
            "carousel_places" : [
                "https://www.corriereadriatico.it/photos/MED/99/06/3489906_1511_mdp_0085.jpg",
                "https://giornaledeinavigli.it/media/2019/11/Audi-RS-Q3_001.jpg",
                "https://www.bonaldi.it/data_files/lookbookModel/20210121174159_6009aed781107_A199279_overfull.jpg"
            ]
        }
    """

    val audiDrivingExperience = """
        {
            "id": "1",
            "image_ade": "https://minervamkt.com/wp-content/uploads/caseta_nieve-980x490.png",
            "title_ade": "L'intelligent performance sui nuovo modelli Audi.",
            "subtitle_ade": "Winter Season 2020 2021",
            "description": "Il modello più potente della gamma RS a Madonna di Campiglio: un territorio ricco di luoghi suggestivi e paesaggi naturali che offre un’inedita cornice per scoprire design, innovazione, sostenibilità e progresso. Questa estate, la Casa dei quattro anelli ti accompagnerà in un’esperienza unica per conoscere Audi RS e-tron GT, la Gran Turismo 100% elettrica, incredibilmente potente e innovativa. Lungo le strade panoramiche che dal lago di Garda portano a Madonna di Campiglio, potrai testare a pieno le performance e le qualità dinamiche di questa vera rivoluzione a quattro ruote, nel pieno rispetto dell’ambiente circostante. A tua disposizione, due pacchetti differenti per durata: uno di una giornata intera, l’altro di una giornata e mezza. In entrambe le esperienze testerai Audi RS e-tron GT su un percorso appositamente disegnato, affiancato da professioni esperti che ti forniranno consigli per migliorare la tua tecnica di guida e in totale sicurezza. Inoltre, potrai scoprire a pieno una delle perle delle Dolomiti, ammirandone le bellezze e gustandone i prodotti locali.",
            "main_title": "Audi Driving Experience",
            "carousel_ade" : [
                "https://www.corriereadriatico.it/photos/MED/99/06/3489906_1511_mdp_0085.jpg",
                "https://giornaledeinavigli.it/media/2019/11/Audi-RS-Q3_001.jpg",
                "https://www.bonaldi.it/data_files/lookbookModel/20210121174159_6009aed781107_A199279_overfull.jpg"
            ]
        }
    """

    fun readPlaceJSON() {
        val placeJSON = JSONObject(place)

        val id = placeJSON.getString("id")

        val image_places = placeJSON.getString("image_places")

        val title = placeJSON.getString("title")

        val subtitle_places = placeJSON.getString("subtitle_places")
        val title_places = placeJSON.getString("title_places")
        val description = placeJSON.getString("description")

        val carousel_places = arrayListOf<String>()
        val carousel = placeJSON.getJSONArray("carousel_places")
        carousel.let {
            (0 until it.length()).forEach {
                val carouselString = carousel.getString(it)
                carousel_places.add(carouselString as String)
            }
        }

        placeObject = AllDataForTest.Places(id, title, subtitle_places, title_places, description, carousel_places, image_places)
    }

    fun readAudiDrivingExperienceJSON() {
        val audiDrivingExperienceJSON = JSONObject(audiDrivingExperience)

        val id = audiDrivingExperienceJSON.getString("id")

        val image_ade = audiDrivingExperienceJSON.getString("image_ade")
        val main_title = audiDrivingExperienceJSON.getString("main_title")
        val title_ade = audiDrivingExperienceJSON.getString("title_ade")
        val subtitle_ade = audiDrivingExperienceJSON.getString("subtitle_ade")

        val description = audiDrivingExperienceJSON.getString("description")

        val carousel_ade = arrayListOf<String>()
        val carousel = audiDrivingExperienceJSON.getJSONArray("carousel_ade")
        carousel.let {
            (0 until it.length()).forEach {
                val carouselString = carousel.getString(it)
                carousel_ade.add(carouselString as String)
            }
        }

        audiDrivingExperienceObject = AllDataForTest.AudiDrivingExperience(id, description, subtitle_ade, main_title, title_ade, carousel_ade, image_ade)
    }


    val foodExperience: String = """
            {
   "id":"",
   "header":"Food Experience",
   "title":"Menu",
   "subtitle":"Scopri le proposte gastronomiche a te riservate",
   "image":"https://figuringoutfoodpod.files.wordpress.com/2019/04/4copper-branch-1600x1172-1080x675.jpg",
   "programExperience":[
      {
         "day":"28-02",
         "start":"07:30",
         "type":"Colazione",
         "activity":"Colazione del teritorio Rifugio Boch",
         "site":"Hotel",
         "description":"Dolci,pane e marmellate  rigorosamente fatti in casa ,latte e yogurt,provinienti dalla lorem ipsum lorem ipsum lorem ipsum ",
         "food":"Tartellette di confetture bio",
         "allergens":[{
         "name":"lattosion"
         },
         {
         "name":"lattosion"
         },
         {
         "name":"frutta a guscio"
         }
         ]
      },
      {
         "day":"28-02",
         "start":"17:30",
         "type":"Pranzo",
         "activity":"Pranzo del teritorio Rifugio Boch",
         "site":"Hotel",
         "description":"Dolci,pane e marmellate  rigorosamente fatti in casa ,latte e yogurt,provinienti dalla lorem ipsum lorem ipsum lorem ipsum ",
         "food":"Tartellette di confetture bio",
         "allergens":[
           {
         "name":"lattosion"
         },
         {
         "name":"lattosion"
         },
         {
         "name":"frutta a guscio"
         }
         ]
      },
      {
         "day":"28-02",
         "start":"17:30",
         "type":"Cena",
         "activity":"Cena del teritorio Rifugio Boch",
         "site":"Hotel",
         "description":"Dolci,pane e marmellate  rigorosamente fatti in casa ,latte e yogurt,provinienti dalla lorem ipsum lorem ipsum lorem ipsum ",
         "food":"Tartellette di confetture bio",
         "allergens":[
           {
         "name":"lattosion"
         },
         {
         "name":"lattosion"
         },
         {
         "name":"frutta a guscio"
         }
         ]
      },
      {
         "day":"29-02",
         "start":"08:30",
         "type":"Colazione",
         "activity":"Colazione del teritorio Rifugio Boch",
         "site":"Hotel",
         "description":"Dolci,pane e marmellate  rigorosamente fatti in casa ,latte e yogurt,provinienti dalla lorem ipsum lorem ipsum lorem ipsum ",
         "food":"Tartellette di confetture bio",
         "allergens":[
           {
         "name":"lattosion"
         },
         {
         "name":"lattosion"
         },
         {
         "name":"frutta a guscio"
         }
            
         ]
      }
   ]
}                                     
        """.trimIndent()

    fun readFoodExperienceJson() {
        val json = JSONObject(foodExperience)


        val id = json.getString("id")
        val header = json.getString("header")
        val title = json.getString("title")
        val subtitle = json.getString("subtitle")
        val image = json.getString("image")


        val jsonArrayProgramExperience = json.getJSONArray("programExperience")
        jsonArrayProgramExperience.let {
            (0 until it.length()).forEach {
                val programExperienceObject = jsonArrayProgramExperience.getJSONObject(it)
                val day = programExperienceObject.getString("day")
                val start = programExperienceObject.getString("start")
                val type = programExperienceObject.getString("type")
                val activity = programExperienceObject.getString("activity")
                val site = programExperienceObject.getString("site")
                val description = programExperienceObject.getString("description")
                val food = programExperienceObject.getString("food")
                val allergens = programExperienceObject.getJSONArray("allergens")
                allergens.let {
                    (0 until it.length()).forEach {
                        val allergen = allergens.getJSONObject(it)
                        val name = allergen.getString("name")
                        val allergenObj = AllDataForTest.Allergens(name)
                        DataGlobal.allergensList.add(allergenObj)
                    }
                }


                val programExperience = AllDataForTest.ProgramExperience(
                    day,
                    start,
                    type,
                    activity,
                    site,
                    description,
                    food,
                    DataGlobal.allergensList
                )
                DataGlobal.programExperienceList.add(programExperience)

                val foodExperience = AllDataForTest.FoodExperience(
                    id,
                    header,
                    title,
                    subtitle,
                    image,
                    DataGlobal.programExperienceList
                )

                DataGlobal.foodExperienceList.add(foodExperience)
            }
        }

    }


    val infoAndContactString = """
        {
            "id": "1",
            "free_text": "Per accedere all'attivita di sci s'alpinismo e necessario essere in grado di scendere una pista rossa senza difficolta e avere una preparazione atletica che permetta di affrontare un dislivello positivo di almeno 750mt. in una sessione unica.
                        \n\nL'abbigliamento e l'attrezzatura di sci alpinismo non sono inclusi.\n\nSono obbligatori: sci con attacchi per sci alpinismo, scarponi da sci alpinismo, pelli, casco.\n\nPer shi ha mai fatto questa attivita, il kit antivalanda (pala, sonda e artva) e disponibile su richiesta ed a nollegio.",
            "image_background": "https://audimediacenter-a.akamaihd.net/system/production/media/99949/images/a7d4d13cf6e2d6105f908420c395e1ba077ca6f6/A211724_full.jpg?1617978672",
            "contact_list": [
                {
                    "name": "Segreteria Organizzativa",
                    "place": "",
                    "street": "",
                    "region": "",
                    "phone": "+39 0236705020",
                    "phone_fix": "",
                    "email": "booking@mkeventi.com"
                },
                {
                    "name": "",
                    "place": "Boutique Hodel Diana 4S",
                    "street": "Via Cima Tosa, 52",
                    "region": "38086 Madonna di Campiglio (TN)",
                    "phone": "+39 0465 441011",
                    "phone_fix": "",
                    "email": "info@hoteldiana.net"
                },
                {
                    "name": "",
                    "place": "Alpen Suite Hotel",
                    "street": "viale Dolomiti di Brenta 84",
                    "region": "38086 Madonna di Campiglio (TN)",
                    "phone": "+39 0465 440100",
                    "phone_fix": "+39 0465 440409",
                    "email": "info@alpensuitehotel.it"
                }
            ]
        }
    """.trimIndent()

    val notificationString = """
        {
        "notification_list": [
            {
                "date": "04/04/2021",
                "notifications": [
                    {
                        "title": "Survey disponibile",
                        "time": "18:00",
                        "description": "Abbiamo bisogno del tuo aiuto per capire come migliorare i prossimi Eventi Audi. Partecipa al sondaggio che abbiamo attiato nell'app"
                    },
                    {
                        "title": "Consigli dello staff",
                        "time": "16:00",
                        "description": "Ricordati di portare le Ciaspole"
                    },
                    {
                        "title": "Aggiornamento Programma",
                        "time": "10:00",
                        "description": "Le attiita di Sabato sono state aggiornate. Controlla cosa e cambiato nella sezione dell'app"
                    }
                ]
            },
            {
                "date": "29/03/2021",
                "notifications": [
                    {
                        "title": "Consigli dello staff",
                        "time": "12:00",
                        "description": "Ricordati di passare al punto di accoglienza per registrare la tua partecipazione. Trovi i riferimenti su dove trovarlo nella sezione Contatti dell'app"
                    }
                ]
            }
        ]
        }
    """.trimIndent()


    fun readContactAndInfoJSON() {
        val contact_list: ArrayList<AllDataForTest.Contact> = arrayListOf()

        val json = JSONObject(infoAndContactString)

        val id = json.getString("id")
        val free_text = json.getString("free_text")
        val image_background = json.getString("image_background")

        val contact_list_json = json.getJSONArray("contact_list")
        contact_list_json.let {
            (0 until it.length()).forEach {
                val contact = contact_list_json.getJSONObject(it)

                val name = contact.getString("name")
                val place = contact.getString("place")
                val street = contact.getString("street")
                val region = contact.getString("region")
                val phone = contact.getString("phone")
                val phone_fix = contact.getString("phone_fix")
                val email = contact.getString("email")

                contact_list.add(AllDataForTest.Contact(name, place, street, region, phone, phone_fix, email))
            }
        }

        val contactAndInfoObject = AllDataForTest.ContactsAndInfo(id, free_text, image_background, contact_list)
    }

    fun readNotificationJSON() {
        lateinit var notificationsObject: AllDataForTest.Notifications

        val json = JSONObject(notificationString)

        val notification_list = json.getJSONArray("notification_list")
        notification_list.let {
            (0 until it.length()).forEach {
                val notificationGroup = notification_list.getJSONObject(it)

                val date = notificationGroup.getString("date")

                val notifications = notificationGroup.getJSONArray("notifications")
                notifications.let{
                    (0 until it.length()).forEach {
                        val notification = notifications.getJSONObject(it)

                        val title = notification.getString("title")
                        val time = notification.getString("time")
                        val description = notification.getString("description")

                        notificationsObject = AllDataForTest.Notifications(date, title, time, description)
                        DataGlobal.notificationList.add(notificationsObject)
                    }
                }
            }
        }
    }


    inner class loadImageFromURL : AsyncTask<String, Void, Bitmap?>() {
        override fun doInBackground(vararg params: String?): Bitmap? {
            var image: Bitmap? = null
            val url = params[0]
            try {
                val imageFromJson = URL(url).openStream()
                image = BitmapFactory.decodeStream(imageFromJson)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return image
        }
        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)
            image = result
        }
    }
}


