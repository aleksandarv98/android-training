package com.eng.audievent

data class EventContact(
    val id: String,
    val title: String,
    val freeText: FreeText,
    val contactImage: BackgroundImage
)

data class FreeText(
    val value: String,
    val format: String
)