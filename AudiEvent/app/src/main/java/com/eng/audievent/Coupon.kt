package com.eng.audievent

interface Coupon {
    val coupon: String
    val eventId: String
    val status: CouponStatus
    val valid: Boolean
    val result: Result
    val resultCode: String
    val resultMessage: String
}