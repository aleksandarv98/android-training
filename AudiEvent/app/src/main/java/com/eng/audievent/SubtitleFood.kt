package com.eng.audievent

data class SubtitleFood(val value: String,
                        val format: String,
                        val processed: String)
