package com.eng.audievent

data class CouponValidation(
    override val coupon: String,
    override val eventId: String,
    override val status: CouponStatus,
    override val valid: Boolean,
    override val result: Result,
    override val resultCode: String,
    override val resultMessage: String,

    val action: String
) : Coupon
