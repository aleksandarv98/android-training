package com.eng.audievent

import java.time.LocalDate

data class PublicEvent(
    val id: String,
    val title: String,
    val description: PublicEventDescription,
    val eventDate: LocalDate,
    val linkMyAudi: LinkMyAudi,
    val priority: Int,
    val status: String,
    val backgroundImage: BackgroundImage) {
}

data class PublicEventDescription(
    override val format: String,
    override val value: String
    ) : EventDescription {
}

data class LinkMyAudi(
    val uri: String,
    val title: String,
    val options: ArrayList<String>
)

class Option {
}
