package com.eng.audievent

data class PushNotification(
    val status: String,
    val statusCode: String,
    val sentTime: String,
    val title: String,
    val topic: String,
    val deepLink: String,
    val body: String
)