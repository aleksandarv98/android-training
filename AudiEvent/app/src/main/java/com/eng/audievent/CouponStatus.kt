package com.eng.audievent

enum class CouponStatus(val value: String) {
    INACTIVE("INACTIVE"),
    ACTIVE("ACTIVE"),
    USED("USED")
}