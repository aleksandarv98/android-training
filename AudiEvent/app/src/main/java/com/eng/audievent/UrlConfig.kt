package com.eng.audievent

import android.os.AsyncTask
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL

object UrlConfig {
    const val root = "http://mobileapp-coll.engds.it/AudiEventi/"
    const val configJson_ep = "config/config.json"
    const val langs_ep = "langs/it_IT.json"

    var langsJsonDataMap : MutableMap<String, String> = mutableMapOf()

    fun getLangsUrl() : String {
        return root + langs_ep
    }
    fun loadJsonDataFromApi(){
        GetJson(getLangsUrl()).execute(getLangsUrl())
    }

    class GetJson(val dataUrl : String) : AsyncTask<String, String, String>() {
        override fun doInBackground(vararg params: String?): String {
            val json: String
            val connection = URL(params[0]).openConnection() as HttpURLConnection

            try {
                connection.connect()
                json = connection.inputStream.use {
                    it.reader().use { inputStreamReader ->
                        inputStreamReader.readText()
                    }
                }
                println(json)
            } finally {
                connection.disconnect()
            }
            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            if (dataUrl == getLangsUrl()) {
                val jsonObject = JSONObject(result!!)

                for (key in jsonObject.keys()) {
                    langsJsonDataMap[key] = jsonObject.getString(key)
                }
            }
        }
    }
}