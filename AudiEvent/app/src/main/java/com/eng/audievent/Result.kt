package com.eng.audievent

enum class Result(val value: String) {
    OK("OK"),
    KO("KO")
}