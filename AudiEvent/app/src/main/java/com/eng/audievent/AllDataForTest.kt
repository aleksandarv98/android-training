package com.eng.audievent

class AllDataForTest {
    data class CalendarEvent(
        val title: String,
        val description: String,
        val image: String
    ) //napraviti listu eventova
    //strana broj 10 skinuti 5 slika

    data class Allergens(val name: String)

    data class FoodExperience(
        val id: String,
        val title: String,
        val header: String,
        val subtitleFood: String,
        val imageBackground: String,
        val programExperience: ArrayList<ProgramExperience> = arrayListOf()
    )

    data class ProgramExperience(
        val day: String,
        val start: String,
        val type: String,
        val activity: String,
        val site: String,
        val description: String,
        val food: String,
        val allergens: ArrayList<Allergens> = arrayListOf()
    )

    data class Places(
        val id: String,
        val title: String,
        val subtitle_places: String,
        val title_places: String,
        val description: String,
        val carousel_places: ArrayList<String> = arrayListOf(),
        val image_places: String
    )

    data class AudiDrivingExperience(
        val id: String,
        val description: String,
        val subtitle_ade: String,
        val main_title: String,
        val title_ade: String,
        val carousel_ade: ArrayList<String> = arrayListOf(),
        val image_ade: String
    )

    data class Coupon(
        val action: String?,
        val coupon: String,
        val eventId: String,
        val status: String,
        val eventStatus: String?,
        val valId: Boolean,
        val result: String,
        val resultCode: String,
    )

    data class ContactsAndInfo(
        val id: String,
        //val title: String,
        val free_text: String,
        val imageBackground: String,
        val contactList: ArrayList<Contact> = arrayListOf()
    )

    data class Contact(
        val name: String,
        val place: String,
        val street: String,
        val region: String,
        val phone: String,
        val phone_fix: String,
        val email: String
    )

    data class Notifications(
        val date: String,
        val title: String,
        val time: String,
        val description: String

    )

    data class Activity(
        val start: String,
        val end: String,
        val activityName: String,
        )

    data class DailyActivity(
        val day: String,
        val activityList: ArrayList<Activity> = arrayListOf()
    )

    data class Program(
        val image: String,
        val title: String,
        val subTitle:String,
        val note:String,
        val description: String,
        val infoUri:String,
        val dailyActivityList: ArrayList<DailyActivity> = arrayListOf()
    )
}