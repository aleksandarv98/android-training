package com.eng.audievent

data class AudiDrivingExperience(
    val id: String,
    val freeText: FreeText,
    val adeSubtitle: String,
    val adeTitle: String,
    val adeCarousel: ArrayList<BackgroundImage>,
    val adeImage: BackgroundImage
)