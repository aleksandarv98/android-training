package com.eng.audievent

import android.os.AsyncTask
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL

object JsonData {

    fun writeData(downloadJSON: String?) {
        val jsonObject = JSONObject(downloadJSON)

        val versioning = jsonObject.getJSONArray("versioning")
        versioning.let {
            (0 until it.length()).forEach {
                val listObject = versioning.getJSONObject(it)

                val platform = listObject.getString("platform")
                val lastVersion = listObject.getString("lastVersion")
                val isMandatoryUpdate = listObject.getBoolean("isMandatoryUpdate")
                val storeLink = listObject.getString("storeLink")
            }
        }
        val backend = jsonObject.getJSONObject("backend")
        val api = backend.getJSONArray("api")
        api.let {
            (0 until it.length()).forEach {
                val listObject = api.getJSONObject(it)

                val name = listObject.getString("name")
                val method = listObject.getString("method")
                val timeout = listObject.getInt("timeout")
                val headers = listObject.getJSONObject("headers")
                val contentType = headers.getString("Content-Type")
            }
        }
    }
}