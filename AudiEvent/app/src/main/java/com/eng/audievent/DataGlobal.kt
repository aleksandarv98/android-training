package com.eng.audievent

object DataGlobal {
    val publicEventsList: ArrayList<PublicEvent> = arrayListOf()
    val calendarEventsList: ArrayList<AllDataForTest.CalendarEvent> = arrayListOf()
    val allergensList: ArrayList<AllDataForTest.Allergens> = arrayListOf()
    val programExperienceList: ArrayList<AllDataForTest.ProgramExperience> = arrayListOf()
    val foodExperienceList: ArrayList<AllDataForTest.FoodExperience> = arrayListOf()
    val notificationList: ArrayList<AllDataForTest.Notifications> = arrayListOf()
}