package com.eng.audievent

interface EventDescription {
    val format: String
    val value: String
}